FROM rust

RUN rustup component add rustfmt && \
    apt update && \
    apt install -y npm protobuf-compiler openjdk-11-jdk-headless && \
    mkdir /opt/openapi && \
    cd /opt/openapi && \
    npm install -g @openapitools/openapi-generator-cli
